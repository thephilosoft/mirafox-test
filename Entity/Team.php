<?php

namespace Entity;

class Team
{
    protected $name;
    protected $totalGames = 0;
    protected $victories = 0;
    protected $draws = 0;
    protected $loses = 0;
    protected $goals = 0;
    protected $missedGoals = 0;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalGames()
    {
        return $this->totalGames;
    }

    /**
     * @param int $totalGames
     * @return Team
     */
    public function setTotalGames($totalGames)
    {
        $this->totalGames = $totalGames;
        return $this;
    }

    /**
     * @return int
     */
    public function getVictories()
    {
        return $this->victories;
    }

    /**
     * @param int $victories
     * @return Team
     */
    public function setVictories($victories)
    {
        $this->victories = $victories;
        return $this;
    }

    /**
     * @return int
     */
    public function getDraws()
    {
        return $this->draws;
    }

    /**
     * @param int $draws
     * @return Team
     */
    public function setDraws($draws)
    {
        $this->draws = $draws;
        return $this;
    }

    /**
     * @return int
     */
    public function getLoses()
    {
        return $this->loses;
    }

    /**
     * @param int $loses
     * @return Team
     */
    public function setLoses($loses)
    {
        $this->loses = $loses;
        return $this;
    }

    /**
     * @return int
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * @param int $goals
     * @return Team
     */
    public function setGoals($goals)
    {
        $this->goals = $goals;
        return $this;
    }

    /**
     * @return int
     */
    public function getMissedGoals()
    {
        return $this->missedGoals;
    }

    /**
     * @param int $missedGoals
     * @return Team
     */
    public function setMissedGoals($missedGoals)
    {
        $this->missedGoals = $missedGoals;
        return $this;
    }

    /**
     * @return float
     */
    public function getAttackPower()
    {
        return $this->goals / $this->totalGames
            * (($this->victories / $this->totalGames)
            + $this->draws / $this->totalGames);
    }

    public function getDefencePower()
    {
        return ($this->missedGoals / $this->totalGames
            * ($this->loses / $this->totalGames
            + $this->draws / $this->totalGames)
        );
    }
}
