<?php

use Formatter\Console;
use Formatter\Html;

spl_autoload_register(function ($className) {
    $file = __DIR__ . "/" . str_replace(
            "\\",
            "/",
            "{$className}.php"
        );

    if (is_readable($file)) {
        include $file;
    }
});

switch (PHP_SAPI) {
    case "cli":
        $formatter = new Console();
        break;

    default:
        $formatter = new Html();
        break;
}

if ($formatter instanceof Html) {
    include "header.php";
}

if (isset($_GET["show"]) || (isset($argv[1]) && $argv[1] == "show")) {
    $repository = new \Repository\CsvTeamRepository(__DIR__ . "/data.csv", ",");
    $allCommands = $repository->getAll();

    $championship = new Championship($allCommands, new MatchDecider($formatter), $formatter);
    $championship->run();
} else {
    $formatter->printUsage();
}

if ($formatter instanceof Html) {
    include "footer.php";
}
