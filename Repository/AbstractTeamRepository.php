<?php

namespace  Repository;

abstract class AbstractTeamRepository
{
    abstract public function getAll();
}
