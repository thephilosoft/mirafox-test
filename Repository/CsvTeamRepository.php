<?php

namespace Repository;

use Entity\Team;

class CsvTeamRepository extends AbstractTeamRepository
{
    protected $file;
    protected $delimiter = ";";

    public function __construct($file, $delimiter = ";")
    {
        if (!empty($file) && is_readable($file)) {
            $this->file = fopen($file, "r");
        } else {
            throw new \InvalidArgumentException("file [{$file}] isn't readable");
        }

        $this->delimiter = $delimiter;
    }

    protected function getLine()
    {
        return fgetcsv($this->file, 0, $this->delimiter);
    }

    public function getAll()
    {
        $result = [];

        // skip header line
        $row = $this->getLine();
        while ($row = $this->getLine()) {
            $command = new Team();
            $kicksAndMisses = array_map(
                "trim",
                explode("-", $row[5])
            );

            $command->setName(trim(iconv("cp1251", "utf-8", $row[0])))
                ->setTotalGames($row[1])
                ->setVictories($row[2])
                ->setDraws($row[3])
                ->setLoses($row[4])
                ->setGoals($kicksAndMisses[0])
                ->setMissedGoals($kicksAndMisses[1])
            ;

            $result[] = $command;
        }

        return $result;
    }
}
