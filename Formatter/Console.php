<?php

namespace Formatter;

use Contracts\FormatterInterface;
use Entity\Team;

class Console implements FormatterInterface
{
    public function printPreMatch(Team $team1, Team $team2)
    {
        printf(
            "%s vs %s\n",
            $team1->getName(),
            $team2->getName()
        );
    }

    public function printRoundBanner($countTeams = 0)
    {
        $banner = "";
        switch ($countTeams) {
            case 2:
                $banner = "finale";
                break;

            case 4:
                $banner = "semi-finale";
                break;

            default:
                break;
        }

        if (empty($banner)) {
            printf("=== %d / %d ===\n\n", 1, $countTeams / 2);
        } else {
            printf("=== %s ===\n\n", $banner);
        }
    }

    public function printChampion(Team $winner)
    {
        echo "mega winner is {$winner->getName()}\n";
    }

    public function printMatchWinner(Team $winner)
    {
        echo "\twinner is: {$winner->getName()}\n";
    }

    /**
     * @param Team[] $teams
     */
    public function printStatTable($teams)
    {
        foreach ($teams as $team) {
            printf(
                "%s ATK = %.2f | DFN = %.2f\n",
                $this->mb_str_pad($team->getName(), 25),
                $team->getAttackPower(),
                $team->getDefencePower()
            );
        }
    }

    public function printUsage()
    {
        echo "execute script with 'show' as and argv[1] param\n";
    }

    public function printScore($score1, $score2)
    {
        echo "\tScore: {$score1}:{$score2}\n";
    }

    protected function mb_str_pad($str, $pad_len, $pad_str = ' ', $dir = STR_PAD_RIGHT, $encoding = NULL){
        $encoding = $encoding === NULL ? mb_internal_encoding() : $encoding;
        $padBefore = $dir === STR_PAD_BOTH || $dir === STR_PAD_LEFT;
        $padAfter = $dir === STR_PAD_BOTH || $dir === STR_PAD_RIGHT;
        $pad_len -= mb_strlen($str, $encoding);
        $targetLen = $padBefore && $padAfter ? $pad_len / 2 : $pad_len;
        $strToRepeatLen = mb_strlen($pad_str, $encoding);
        $repeatTimes = ceil($targetLen / $strToRepeatLen);
        $repeatedString = str_repeat($pad_str, max(0, $repeatTimes)); // safe if used with valid utf-8 strings
        $before = $padBefore ? mb_substr($repeatedString, 0, floor($targetLen), $encoding) : '';
        $after = $padAfter ? mb_substr($repeatedString, 0, ceil($targetLen), $encoding) : '';
        return $before . $str . $after;
    }
}
