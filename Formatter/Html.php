<?php

namespace Formatter;

use Contracts\FormatterInterface;
use Entity\Team;

class Html implements FormatterInterface
{
    public function printPreMatch(Team $team1, Team $team2)
    {
        echo "<h4>{$team1->getName()} vs {$team2->getName()}</h4>";
    }

    public function printMatchWinner(Team $winner)
    {
        echo "<p><b>Match winner is:</b> {$winner->getName()}</p>";
    }

    public function printChampion(Team $winner)
    {
        echo "<div class='card'><h2>Mega champ is {$winner->getName()}</h2></div>";
    }

    public function printRoundBanner($countTeams)
    {
        $banner = "";
        switch ($countTeams) {
            case 2:
                $banner = "finale";
                break;

            case 4:
                $banner = "semi-finale";
                break;

            default:
                $banner = "1 / " . $countTeams / 2;
                break;
        }

        echo "<h2 style=\"text-align: center\">{$banner}</h2>";
    }

    public function printStatTable($teams)
    {
        echo <<<HTML
<table class="table table-striped">
    <thead>
        <tr>
            <th>Название команды</th>
            <th>Атака</th>
            <th>Защита</th>
        </tr>
    </thead>
    <tbody>
HTML;
        foreach ($teams as $team) {
            echo "<tr>
    <td>{$team->getName()}</td>
    <td>{$team->getAttackPower()}</td>
    <td>{$team->getDefencePower()}</td>
</tr>
";
        }
        echo <<<HTML
    </tbody>
</table>
HTML;
    }

    public function printUsage()
    {
        echo "<a href='/?show' class='btn btn-primary'>Запустить чемпионат</a>";
    }

    public function printScore($score1, $score2)
    {
        echo "<p><b>Счёт: </b> {$score1} : {$score2}</p>";
    }
}
