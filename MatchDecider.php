<?php

use Contracts\DeciderInterface;
use Entity\Team;

class MatchDecider implements DeciderInterface
{
    protected $formatter;

    public function __construct(\Contracts\FormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * @param Team $command1
     * @param Team $command2
     * @return Team
     */
    public function decide(Team $command1, Team $command2)
    {
        $score1 = $command1->getAttackPower() + $command2->getDefencePower();
        $score2 = $command2->getAttackPower() + $command1->getDefencePower();

        $this->formatter->printScore(
            round($score1),
            round($score2)
        );

        if ($score1 >= $score2) {
            $winner = $command1;
        } else {
            $winner = $command2;
        }

        return $winner;
    }
}
