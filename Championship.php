<?php

use Contracts\DeciderInterface;
use Contracts\FormatterInterface;
use Entity\Team;

class Championship
{
    protected $teams = [];
    protected $matchups = [];
    protected $decider;
    protected $formatter;

    public function __construct(
        array $teams,
        DeciderInterface $decider,
        FormatterInterface $formatter
    ) {
        $this->teams = $teams;
        $this->decider = $decider;
        $this->formatter = $formatter;
    }

    public function run()
    {
        $this->formatter->printStatTable($this->teams);
        while (count($this->teams) > 1) {
            $this->formatter->printRoundBanner(count($this->teams));
            $this->generateMatchups();
            foreach ($this->getMatchups() as $matchup) {
                $this->formatter->printPreMatch($matchup[0], $matchup[1]);

                $winner = $this->decider->decide($matchup[0], $matchup[1]);

                $this->formatter->printMatchWinner($winner);

                $this->teams[] = $winner;
            }
        }

        $this->formatter->printChampion($this->teams[0]);
    }

    protected function generateMatchups()
    {
        $this->matchups = [];
        if (count($this->teams) >= 2) {
            while (count($this->teams) >= 2) {
                $this->matchups[] = [
                    $this->getRandomTeam(),
                    $this->getRandomTeam()
                ];
            }
        }
    }

    public function getMatchups()
    {
        return $this->matchups;
    }

    /**
     * @return Team
     */
    protected function getRandomTeam()
    {
        $index = mt_rand(0, count($this->teams) - 1);
        $team = $this->teams[$index];
        unset($this->teams[$index]);
        $this->teams = array_values($this->teams);

        return $team;
    }
}
