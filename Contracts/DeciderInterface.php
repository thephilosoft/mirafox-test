<?php

namespace Contracts;

use Entity\Team;

interface DeciderInterface
{
    public function decide(Team $team1, Team $team2);
}
