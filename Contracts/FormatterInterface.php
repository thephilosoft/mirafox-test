<?php

namespace Contracts;

use Entity\Team;

interface FormatterInterface
{
    public function printPreMatch(Team $team1, Team $team2);

    public function printMatchWinner(Team $winner);

    public function printChampion(Team $winner);

    public function printRoundBanner($countTeams);

    public function printStatTable($teams);

    public function printUsage();

    public function printScore($score1, $score2);
}
